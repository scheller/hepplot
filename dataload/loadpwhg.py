from .Plotfiles import Plotfile_Pwhg as pfpwhg
from .Hepplot import Hepplot as hp
import numpy as np

# Take a Plotfile_Powheg object and extract the desired plot
# returns a hepplot object
# pfile: Plotfile_Powheg object
# obsname: str name of the observable to return
def loadpwhgobs(pfile: pfpwhg.Plotfile_Pwhg, obsname: str):
    if not pfile.isin(obsname):
        raise RuntimeError('Observable not found: '+obsname)
    obsindex=pfile.getplot(obsname) # Plotfile_Powheg returns an index tuple here!
    lines=pfile.getfile().readlines()[obsindex[0]:obsindex[1]]
    tmparray=np.empty((0,4))
    for line in lines:
        linec=line.replace("D+","e+").replace("D-","e-")
        # read file linewise into np.array
        # add them to tmparray
        tmparray=np.vstack((tmparray,np.fromstring(linec,sep=' ')))
    # slice tmparray/insert data
    hepp=hp()
    data=tmparray
    hepp.setx((data[:,0]+data[:,1])/2)
    # TODO non-hardcoded scale factor
    hepp.sety(1000.0*data[:,2])
    hepp.setey(1000.0*data[:,3])
    # Use bin width as x-error
    hepp.setex((data[:,1]-data[:,0])/2)
    hepp.settitle('POWHEG: '+pfile.getname())
    return hepp    
