# TODO __enter__ and __exit__ functions (if necessary)
import os
class Plotfile:
    def __init__(self, fpath: str):
        self.filepath=fpath
        self.plots={}
        
    def isin(self, obsname: str):
        return (obsname in self.plots)
        
    def getplot(self, obsname: str):
        if self.isin(obsname):
            return self.plots[obsname]
            
    def showavail(self):
        return list(self.plots.keys())
        
    def getpath(self):
        return self.filepath
    
    def getname(self):
        return os.path.basename(self.filepath)
