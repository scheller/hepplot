from .Plotfiles import Plotfile_Hawk as pfhwk
from .Hepplot import Hepplot as hp
import numpy as np

# Take a Plotfile_Hawk object and extract the desired plot in the desired mode
# returns a hepplot object
# pfile: Plotfile_Hawk object
# obsname: str name of the observable to return
# mode: str, can be either 'LO', 'NLO, 'EW', 'QCD', 'IP' or 'IG'
def loadhawkobs(pfile: pfhwk.Plotfile_Hawk, obsname: str, mode: str):
    if pfile.isin(obsname):
        try:
            columnadd = {'LO':0, 'NLO':2,'EW':4,'QCD':6,'IP':8,'IG':10}[mode]
        except KeyError as exc:
            raise RuntimeError('Invalid mode for HAWK plotting: '+mode) from exc
    else:
        raise RuntimeError('Observable not found: '+obsname)
    ffile = pfile.getplot(obsname) # Plotfile_Hawk returns a file here!
    ffile.seek(0)
    data = None
    data = np.loadtxt(ffile)
    hepp = hp()
    binwidth = data[1,0]-data[0,0] # TODO could lead to out of bounds error...
    hepp.setx(data[:,0]+0.5*binwidth)
    # TODO non-hardcoded scale factor?
    hepp.sety(data[:,1+columnadd])
    hepp.setey(data[:,2+columnadd])
    # Use bin width as x-error
    xerr = 0.5*binwidth*np.ones(data[:,0].shape)
    hepp.setex(xerr)
    hepp.settitle('HAWK: '+pfile.getname())
    return hepp
