#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import os.path
from matplotlib.backends.backend_pdf import PdfPages
from dataload.Hepplot import Hepplot as hp
from dataload.Plotfiles import Plotfile as ptf
from dataload.loadhawk import loadhawkobs
from dataload.loadpwhg import loadpwhgobs
from dataload.Plotfiles.Plotfile_Hawk import Plotfile_Hawk as pfhwk
from dataload.Plotfiles.Plotfile_Pwhg import Plotfile_Pwhg as pfpwhg
import dataload.formats as ft
import plotroutines as pr
import userroutines as ur


#
#################
#
# MAIN starts here
#

version='0.1'

#
# Show menu
#
print('Welcome to Hepplotter v'+version+'!\n')


# TODO All of this below can be restructured!

print('Please select the data format of the (first) file you want to plot:')
plottypes=[ur.gettype()]
print('Please enter the path to the (first) file you want to plot:')
plotpaths=[ur.getpath(plottypes[0])]
plotnumber=1
while True:
    choice = input('If you want, you can add more files to compare to the first one now. Do you want to do that? (y/n)\n')
    if choice.lower() == 'y':
        print('\nPlease select the data format of the ' + str(plotnumber+1) + '. file you want to plot:')
        plottypes.append(ur.gettype())
        print('\nPlease enter the path to the ' + str(plotnumber+1) + '. file you want to plot:')
        plotpaths.append(ur.getpath(plottypes[0]))
        plotnumber+=1
    elif choice.lower() == 'n':
        break

# Construct Plotfile objects...
myplotfiles = []
for ttype,ppath in zip(plottypes,plotpaths):
    myplotfiles.append(ft.returnPlotfile(ttype, ppath))
    # ...and initialise with available plots
    myplotfiles[-1].addall()

# Plot one or all from f[0]?
#   one: which?
print('Do you want to print all observables from '+os.path.basename(plotpaths[0])+'?')
while True:
    choice = input('(y/n)\n')
    if choice.lower() == 'y':
        toplot = myplotfiles[0].showavail()
        break
    elif choice.lower() == 'n':
        toplot=[ur.selectwhichplot(myplotfiles[0])]
        break
# Now, toplot is a list of all the plots from file 1 we want to plot...

# TODO titles/labels/axes!
# For selected graph(s) in toplot:
#   retrieve data
histos=[]
for currobs in toplot:
    currhistos=[]
#   try to find equivalent(s)    
    for plot,ptype in zip(myplotfiles,plottypes):
        if plot.isin(currobs):
            # append to Histos of this variable
            currhistos.append(ft.returnLoad(ptype,plot,currobs))
        else:
#       if not found:
#           Ask for which
            print('Cannot find the equivalent to ' + currobs + ' in ' +  plot.getpath())
            print('You have to select another one instead...')
            currhistos.append(ft.returnLoad(ptype,plot,ur.selectwhichplot(plot)))
    histos.append(currhistos)
    
# Now histos is a list of lists, each containing the plot file(s) for one observable
#   plot (together)
#   Append to pdf
with PdfPages('output.pdf') as pdf: # TODO make filename up to user
    for mytitle,histarr in zip(toplot,histos):
        pr.drawhistoplot(histarr,mytitle,'GeV','pb')
        pdf.savefig()
        plt.close()

