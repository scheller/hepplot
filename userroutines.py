#
#################
#
# Routines to get info from user
#
import dataload.formats as ft

# Ask for the data dype of a file to plot
# TODO use more generic funtions!
def gettype():
    print('Available data formats are:\n')
    for index, form in enumerate(ft.formats):
        print('['+str(index+1)+'] : '+ft.names[form]+'\n')
    while True:
        choice = int(input('Which one do you chose? (1-'+str(len(ft.formats))+') '))-1
        if choice in range(len(ft.formats)): # valid choice
            return(ft.formats[choice])

# Ask for the path to a file to plot
# Description differs, depending on file type
def getpath(ptype):
    print(ft.statements[ptype]+'\n')
    return(input('Please enter the path: ')) # TODO Check for validity here
    
def selectwhichplot(myplotfile):
    print('Which one should be plotted?')
    availnames = myplotfile.showavail()
    for index,nname in enumerate(availnames):
        print('['+str(index+1)+'] : '+nname+'\n')
    print('[0] : (none)\n')
    while True:
        choice = int(input('Please select (1-'+str(len(availnames))+') or 0'))-1
        if choice in range(len(availnames)): # valid choice
            return(availnames[choice])
        if choice == -1:
            return ''
            
def yesnoquestion(queststr):
    print(queststr) # queststr has to include the question mark already!
    while True:
        choice = input('(y/n)\n')
        if choice.lower() == 'y':
            return True
        elif choice.lower() == 'n':
            return False
    
    
def selectfromlist(queststr, options, allowNone, namefunction=None):
    print(queststr) # queststr has to include the question mark already!
    for index,obj in enumerate(options):
        if namefunction: # if a function is given, we use this to print the name of the option!
            print('['+str(index+1)+'] : '+obj.namefunction()+'\n')
        else:
            print('['+str(index+1)+'] : '+obj+'\n')
    if allowNone:
        print('[0] : (none)\n')
    while True:
        if allowNone:
            choice = int(input('Please select (1-'+str(len(options))+') or 0: '))-1
        else:
            choice = int(input('Please select (1-'+str(len(options))+')'))-1
        if choice in range(len(options)): # valid choice
            return(options[choice])
        if choice == -1:
            return None
#        TODO adapt for case of two arrays (e.g. gettype)!
