#
#################
#
# Summarizes the interfaces for the currently available data formats
#
from dataload.loadhawk import loadhawkobs
from dataload.loadpwhg import loadpwhgobs
from dataload.Plotfiles.Plotfile_Hawk import Plotfile_Hawk as pfhwk
from dataload.Plotfiles.Plotfile_Pwhg import Plotfile_Pwhg as pfpwhg

formats = ['hwk','pwhg']
names = {'hwk': 'HAWK', 'pwhg': 'POWHEG-BOX'}
statements = { 
    'hwk': 'The path must lead to a DIRECTORY containing the HAWK histograms in multiple "dat.*" files.',
    'pwhg': 'The path must lead to a single file (usually "pwg-NLO.top") containing the histograms in POWHEG-BOX style.'
    }

def returnPlotfile(mytype, fpath):
    if mytype == 'hwk':
        return(pfhwk(fpath))
    elif mytype == 'pwhg':
        return(pfpwhg(fpath))
        
def returnLoad(mytype, pfile, obsname):
    # TODO add option for the different modes with hawk!
    if obsname == '':
        return None
    if mytype == 'hwk':
        return(loadhawkobs(pfile, obsname, 'LO'))
    elif mytype == 'pwhg':
        return(loadpwhgobs(pfile, obsname))

