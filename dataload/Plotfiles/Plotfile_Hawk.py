from .Plotfile import Plotfile
import os
from os.path import join

class Plotfile_Hawk(Plotfile):
    def __init__(self,fpath: str):
        Plotfile.__init__(self,fpath)
        
    def __del__(self):
        for ffile in self.plots.values():
            ffile.close()
            
    def addall(self):
        # Open directory, list files
        for ifile in os.listdir(self.filepath):
            # For every HAWK file: extract path and name, add to list
            if (ifile.lower().startswith('dat.')
                and (not 'weightmax' in ifile.lower())): # fulfills convention for HAWK files
            # TODO what if saved with different convention?
                obsname=ifile.lower().split('.')[1]
                obspath=join(self.filepath,ifile)
                self.addplot(obspath,obsname)
                
        
    def addplot(self,obspath: str,obsname: str):
        if not obsname in self.plots:
            self.plots[obsname]=open(obspath,'r') # TODO we can expect excepts here...
        else: raise RuntimeError('Key already exists: '+obsname+'\nUse Plotfile_Hawk.changeplot instead')
            
    def changeplot(self,obspath: str,obsname: str):
        if obsname in self.plots:
            self.plots[obsname].close()
            self.plots[obsname]=open(obspath,'r') # TODO we can expect excepts here...
        else: raise RuntimeError('Key does not exist: '+obsname+'\nUse Plotfile_Hawk.addplot instead')