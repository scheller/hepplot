from .Plotfile import Plotfile

class Plotfile_Pwhg(Plotfile):
    def __init__(self,fpath: str):
        Plotfile.__init__(self,fpath)
        # For POWHEG file, already open file so it can be returned
        self.myfile=open(self.filepath, 'r') # TODO we have to expect excepts here
        
    def __del__(self):
        self.myfile.close()
        
    def addall(self):
        # Loop over file
        self.myfile.seek(0)
        inBlock = False
        for index, line in enumerate(self.myfile.readlines()):
            # find headings - 'enter' block (= first line)
            if line.startswith('#') and (not inBlock):
                # TODO what if we *are* already in a block?
                l1 = index+1
                # extract title
                keys=line.split()
                del(keys[0]) # is '#'
                del(keys[-1]) # is index
                del(keys[-1]) # is 'index'
                obsname = ' '.join(keys)
                inBlock = True
            if line == '\n' and inBlock:
                # find empty line - 'leave' block (= last line)
                # add it to list
                self.addplot((l1,index),obsname)
                inBlock = False
        
    def addplot(self,iindex: tuple,obsname: str):
        if not obsname in self.plots:
            self.plots[obsname]=iindex
        else: raise RuntimeError('Key already exists: '+obsname+'\nUse Plotfile_Hawk.changeplot instead')
            
    def changeplot(self,iindex: int,obsname: str):
        if obsname in self.plots:
            self.plots[obsname]=iindex
        else: raise RuntimeError('Key does not exist: '+obsname+'\nUse Plotfile_Hawk.addplot instead')

    def getfile(self):
        self.myfile.seek(0)
        return self.myfile

