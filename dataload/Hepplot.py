import numpy as np

class Hepplot:
    def __init__(self):
        
        pass
        
    def setx(self,set_x: np.ndarray):
        self.xvalues=set_x    
    def sety(self,set_y: np.ndarray):
        self.yvalues=set_y
    def setex(self,set_ex: np.ndarray):
        self.xerror=set_ex
    def setey(self,set_ey: np.ndarray):
        self.yerror=set_ey
    def settitle(self,mytitle: str):
        self.title = mytitle
        
    def getx(self):
        return self.xvalues
    def gety(self):
        return self.yvalues
    def getex(self):
        return self.xerror
    def getey(self):
        return self.yerror
    def gettitle(self):
        return self.title
