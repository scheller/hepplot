#
#################
#
# Main plotter routine to return plot
#
def drawhistoplot(plotfiles,title,xlegend,ylegend):
    # TODO Check data types?
    
    # Single or multiple plots?
    # --> include comparison plot
    if len(plotfiles) > 1:
        fig, (p1, p2) = plt.subplots(2,1,gridspec_kw={'height_ratios':[3,1]},sharex=True)
        p2.set_ylabel('ratio')
        p2.set_ylim((0,2))
        p2.grid(b=True,which='major',axis='y')
    else:
        fig = plt.figure()
        p1 = plt.gca()
        
    p1.set_ylabel('[pb]')
    fig.suptitle(title)
    plt.xlabel('[GeV]')
    
    # loop over plots and do for each:
    for index,plotfile in enumerate(plotfiles):
        if not plotfile: # empty file - i.e. not to be plotted!
            continue
        p1.errorbar(plotfile.getx(), plotfile.gety(), yerr = plotfile.getey(),
            xerr = plotfile.getex(),label=plotfile.gettitle(),
            marker = '', ms=.2, lw=.2,drawstyle='steps-mid') # ls = '', marker = '.'


        if index > 0 and (len(plotfile.getx())==len(plotfiles[0].getx())): # Do a comparison plot
            ypsilon=plotfile.gety()/plotfiles[0].gety()
            ery=ypsilon*((plotfile.getey()/plotfile.gety())**2+(plotfiles[0].getey()/plotfiles[0].gety())**2)**(1/2) # Gaussian error propagation
            p2.errorbar(plotfile.getx(),ypsilon,yerr = ery,xerr = plotfile.getex(),
                lw=.2,ms=.2,marker='',drawstyle='steps-mid') # ls = '', marker = '.'
    p1.legend(loc='upper right') # TODO Make this up to the user
    return

#
#################
#
# Helper routine(s)
#
